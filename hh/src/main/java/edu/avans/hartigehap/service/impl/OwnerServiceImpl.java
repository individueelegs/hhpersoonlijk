package edu.avans.hartigehap.service.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import edu.avans.hartigehap.domain.Owner;
import edu.avans.hartigehap.repository.OwnerRepository;
import edu.avans.hartigehap.service.OwnerService;
@Service("ownerService")
@Repository
@Transactional 

public class OwnerServiceImpl implements OwnerService {

	@Autowired
	private OwnerRepository ownerRepository;
	
	@Override
	public List<Owner> findAll() {
		// TODO Auto-generated method stub
		return Lists.newArrayList(ownerRepository.findAll());
	}

	@Override
	public Owner findById(Long id) {
		// TODO Auto-generated method stub
		return ownerRepository.findOne(id);
	}

	@Override
	public List<Owner> findByName(String name) {
		// TODO Auto-generated method stub
		return ownerRepository.findByName(name);
	}

	@Override
	public Owner save(Owner owner) {
		// TODO Auto-generated method stub
		return ownerRepository.save(owner);
	}

	@Override
	public void delete(Owner owner) {
		// TODO Auto-generated method stub
		ownerRepository.delete(owner);
	}
	//ownerRepository.findByRestaurants(Arrays.asList(new Restaurant[]{restaurant}), new Sort(Sort.Direction.ASC, "name"));


}
