package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ConcreteOrderItem extends OrderItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
