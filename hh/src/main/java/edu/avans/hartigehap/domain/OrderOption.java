package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class OrderOption extends DecoratedOrderItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public OrderOption(OrderItem orderItem, MenuItem menuItem,String description, int quantity){
		super(orderItem, menuItem, quantity);
		
	}
	
	

}
