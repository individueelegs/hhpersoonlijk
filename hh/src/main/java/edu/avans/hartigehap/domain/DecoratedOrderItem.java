package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//@Entity


@ToString(callSuper=true, includeFieldNames=true)

@Getter @Setter


@NoArgsConstructor

public abstract class DecoratedOrderItem extends OrderItem {
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	private OrderItem orderItem;
	private int quantity;

	public DecoratedOrderItem(OrderItem toBeDecoratedOrderItem, MenuItem menuItem, int quantity){
		super(menuItem, quantity);
		orderItem = toBeDecoratedOrderItem;
		this.quantity = quantity;
	}
	
	public String description(){
		
		return "decorated order";	
	}
	
	@Transient
	public int getPrice(){
		
		return orderItem.getPrice();
		
	}
	
	
	
	

}
