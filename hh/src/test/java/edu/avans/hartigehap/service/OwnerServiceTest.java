package edu.avans.hartigehap.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.avans.hartigehap.domain.Owner;
import edu.avans.hartigehap.service.testutil.AbstractTransactionRollbackTest;

public class OwnerServiceTest extends AbstractTransactionRollbackTest  {

	@Autowired 
	private OwnerService ownerService;
	
	@Test
	public void create() {
		Owner owner1 = new Owner();
		
		ownerService.save(owner1);
		
	}

}
